from gpiozero import MotionSensor
pir = MotionSensor(4)
import datetime
import time


while True:
	pir.wait_for_motion()
	tsb = time.time()
	stb = datetime.datetime.fromtimestamp(tsb).strftime('%Y-%m-%d %H-%M-%S')

	print('Motion Started ' + stb)
	pir.wait_for_no_motion()
	tse = time.time()
	ste = datetime.datetime.fromtimestamp(tse).strftime('%Y-%m-%d %H-%M-%S')
	print('Motion Stopped ' + ste)
	print('Motion Lasted ' + str(tse - tsb))