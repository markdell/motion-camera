from gpiozero import MotionSensor
from picamera import PiCamera
import subprocess
import os.path
import shlex
import logging
from time import sleep
from datetime import datetime, time, date

pir = MotionSensor(4)
logging.basicConfig(filename='/home/pi/motion/motion.log',level=logging.DEBUG)
camera = PiCamera()

while True:
	day_of_week = date.today().weekday() # 0 is Monday
	now = datetime.now()
	now_time = now.time()
	if day_of_week > 5 or (not (now_time > time(8) and now_time < time(17))):
		logging.debug("Not going to run on " + str(datetime.now()))
		sleep(60 * 30)
		continue
	else:
		logging.debug("we good")
	logging.debug("Waiting for motion")
	pir.wait_for_motion()
	st = str(datetime.now())
	filename = '/home/pi/motion/motion_' + st.replace(":", "_").replace(" ", "_")
	logging.debug('recording into ' + filename + '.h264')
	camera.start_recording(filename + '.h264')
	pir.wait_for_no_motion()
	camera.stop_recording()
	logging.debug('recording stopped')
	
	from subprocess import call
	command = "MP4Box -add " +filename + ".h264 " + filename + ".mp4; rm " + filename + ".h264; rsync --daemon; rsync --remove-source-files --password-file=/home/pi/rsyncd.secret ~/motion/*.mp4 rsync://pi@192.168.86.41:/pi/motion"
	call ([command], shell=True)
